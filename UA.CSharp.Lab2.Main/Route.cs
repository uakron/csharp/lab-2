﻿using System;

namespace UA.CSharp.Lab2.Main
{
    public class Route
    {
        public string PointA { get; set; }

        public string PointB { get; set; }

        public decimal TicketPrice { get; set; }

        public bool RouteCanceled { get; set; }

        public Bus Bus { get; set; }

        public Company Company { get; set; }
    }

    public class RouteCanceledException : Exception
    {
        public RouteCanceledException(string pointA, string pointB)
            : base(string.Format("Route from {0} to {1} is canceled. Transaction cannot be processed.", pointA, pointB))
        { }
    }

    public class InvalidBalanceException : Exception
    {
        public InvalidBalanceException(string customerBalance, string ticketPrice)
            : base(string.Format("Balance of ${0} is not enough to pay for ticket price of ${1}", customerBalance, ticketPrice))
        { }
    }
}
