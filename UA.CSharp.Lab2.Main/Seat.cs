﻿namespace UA.CSharp.Lab2.Main
{
    public class Seat
    {
        public int SeatNumber { get; set; }

        public Status Status { get; set; }

        public Bus Bus { get; set; }
    }

    public enum Status { Free, Occupied }
}
