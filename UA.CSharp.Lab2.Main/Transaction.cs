﻿namespace UA.CSharp.Lab2.Main
{
    public class Transaction
    {
        public Customer Customer { get; set; }

        public Seat Seat { get; set; }

        public Route Route { get; set; }
    }
}
