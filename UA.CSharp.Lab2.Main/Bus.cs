﻿using System;
using System.Collections.Generic;

namespace UA.CSharp.Lab2.Main
{
    public class Bus
    {
        public Guid BusNumber { get; set; }

        public List<Route> Routes { get; set; }

        public int Capacity { get; set; }

        private List<Seat> _seats { get; set; }

        public List<Seat> Seats
        {
            get
            {
                return _seats;
            }
            set
            {
                _seats = new List<Seat>();

                for (var i = 1; i <= Capacity; i++)
                {
                    _seats.Add(new Seat
                    {
                        SeatNumber = i,
                        Status = Status.Free
                    });
                }
            }
        }
    }

    public class UnavailableSeatException : Exception
    {
        public UnavailableSeatException(string busNumber)
            : base(string.Format("Bus #{0} has no seats available", busNumber))
        { }
    }
}
