﻿using System.Collections.Generic;

namespace UA.CSharp.Lab2.Main
{
    public class Company
    {
        public string Name { get; set; }

        public List<Route> Routes { get; set; }
    }
}
