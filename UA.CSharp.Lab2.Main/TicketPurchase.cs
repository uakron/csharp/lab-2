﻿using System;
using System.Linq;

namespace UA.CSharp.Lab2.Main
{
    public class TicketPurchase
    {
        public Transaction ProcessTransaction(Customer customer, Route route)
        {
            try
            {
                var seat = route.Bus.Seats
                    .OrderBy(a => a.SeatNumber)
                    .FirstOrDefault(a => a.Status != Status.Occupied);

                if (route.RouteCanceled)
                    throw new RouteCanceledException(route.PointA, route.PointB);

                if (seat == null)
                    throw new UnavailableSeatException(route.Bus.BusNumber.ToString());

                if (customer.Balance < route.TicketPrice)
                    throw new InvalidBalanceException(customer.Balance.ToString(), route.TicketPrice.ToString());

                return new Transaction
                {
                    Customer = customer,
                    Seat = seat,
                    Route = route
                };
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
