﻿using System.Collections.Generic;

namespace UA.CSharp.Lab2.Main
{
    public class Customer
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public decimal Balance { get; set; }

        public List<Transaction> Transactions { get; set; }
    }
}
