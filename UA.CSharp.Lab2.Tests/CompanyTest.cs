﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UA.CSharp.Lab2.Main;

namespace UA.CSharp.Lab2.Tests
{
    [TestClass]
    public class CompanyTest
    {
        [TestMethod]
        public void RunCompanyTest()
        {
            const int CAPACITY_A = 15, CAPACITY_B = 20;

            var busA = new Bus
            {
                BusNumber = Guid.NewGuid(),
                Capacity = CAPACITY_A,
                Seats = default
            };

            Assert.AreEqual(CAPACITY_A, busA.Capacity);
            Assert.AreEqual(CAPACITY_A, busA.Seats.Count);

            var busB = new Bus
            {
                BusNumber = Guid.NewGuid(),
                Capacity = CAPACITY_B,
                Seats = default
            };

            Assert.AreEqual(CAPACITY_B, busB.Capacity);
            Assert.AreEqual(CAPACITY_B, busB.Seats.Count);

            var routeA = new Route
            {
                PointA = "Polsky",
                PointB = "Library",
                TicketPrice = 1.25m,
                Bus = busA
            };

            Assert.AreEqual("Polsky", routeA.PointA);
            Assert.AreEqual("Library", routeA.PointB);
            Assert.AreEqual(1.25m, routeA.TicketPrice);
            Assert.AreEqual(busA, routeA.Bus);

            var routeB = new Route
            {
                PointA = "Simmons Hall",
                PointB = "West Campus Parking Deck",
                TicketPrice = 1.10m,
                Bus = busB
            };

            Assert.AreEqual("Simmons Hall", routeB.PointA);
            Assert.AreEqual("West Campus Parking Deck", routeB.PointB);
            Assert.AreEqual(1.10m, routeB.TicketPrice);
            Assert.AreEqual(busB, routeB.Bus);

            var routes = new List<Route>() { routeA, routeB };

            var company = new Company
            {
                Name = "Random Bus Company",
                Routes = routes
            };

            Assert.AreEqual("Random Bus Company", company.Name);
            Assert.AreEqual(routes, company.Routes);

            var customer = new Customer
            {
                FirstName = "Bailey",
                LastName = "Parrish",
                Balance = 2.00m
            };

            Transaction transaction;

            // set the route to canceled to test exception
            routeA.RouteCanceled = true;
            Assert.ThrowsException<RouteCanceledException>
                (() => transaction = new TicketPurchase().ProcessTransaction(customer, routeA));
            routeA.RouteCanceled = false;

            // set all of the bus seats for route A to occupied to test exception
            routeA.Bus.Seats.ForEach(a => a.Status = Status.Occupied);
            Assert.ThrowsException<UnavailableSeatException>
                (() => transaction = new TicketPurchase().ProcessTransaction(customer, routeA));

            // set the balance of the customer to less than the ticket price of route B to test exception
            customer.Balance = default;
            Assert.ThrowsException<InvalidBalanceException>
                (() => transaction = new TicketPurchase().ProcessTransaction(customer, routeB));
        }
    }
}
